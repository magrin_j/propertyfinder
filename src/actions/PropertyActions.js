import alt from '../alt';
import { PropertyFetcher } from '../utils/PropertyFetcher';

export class PropertyActions {

    constructor() {
      this.generateActions(
          'updateProperties',
          'propertiesFailed'
      );
    }

    getProperties(key, value) {
      return PropertyFetcher.fetch(key, value)
          .then((response) => {
            if (response.application_response_code.substr(0, 1) === '1') {
              this.updateProperties(response.listings);
            } else {
              this.propertiesFailed('Location not recognized; please try again.');
            }
          })
          .catch((e) => {
            this.propertiesFailed(e);
          });
    }

}

export default alt.createActions(PropertyActions);
