import React, {
    StyleSheet,
    ListView,
    Image,
    View,
    TouchableHighlight,
    Text,
    Component,
} from 'react-native';

import PropertyStore from '../stores/PropertyStore';

const PropertyViewComponent = require('./PropertyView.react');

const styles = StyleSheet.create({
  thumb: {
    width: 80,
    height: 80,
    marginRight: 10,
  },
  textContainer: {
    flex: 1,
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd',
  },
  price: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#48BBEC',
  },
  title: {
    fontSize: 20,
    color: '#656565',
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
  },
});

class SearchResultsComponent extends Component {

    constructor(props) {
      super(props);
      this.state = PropertyStore.getState();
      this.state.dataSource = new ListView.DataSource(
          {rowHasChanged: (r1, r2) => r1.guid !== r2.guid});

      if (this.state.properties) {
        this.state = {
          dataSource: this.state.dataSource.cloneWithRows(this.state.properties),
        };
      }
    }

    componentDidMount() {
      PropertyStore.listen(this.onChange.bind(this));
    }

    componentWillUnmount() {
      PropertyStore.unlisten(this.onChange.bind(this));
    }

    onChange(state) {
      this.setState(state);
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(state.properties),
      });
    }

    rowPressed(guid) {
      this.props.navigator.push({
        title: 'Property',
        component: PropertyViewComponent,
        passProps: { guid: guid },
      });
    }

    renderRow(rowData) {
      const price = rowData.price_formatted.split(' ')[0];

      return (
          <TouchableHighlight onPress={() => this.rowPressed(rowData.guid)}
                              underlayColor="#dddddd">
              <View>
                  <View style={styles.rowContainer}>
                      <Image style={styles.thumb} source={{ uri: rowData.img_url }} />
                      <View  style={styles.textContainer}>
                          <Text style={styles.price}>£{price}</Text>
                          <Text style={styles.title}
                                numberOfLines={1}>{rowData.title}</Text>
                      </View>
                  </View>
                  <View style={styles.separator}/>
              </View>
          </TouchableHighlight>
      );
    }

    render() {
      return (
            <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderRow.bind(this)}/>
            );
    }

}

module.exports = SearchResultsComponent;
