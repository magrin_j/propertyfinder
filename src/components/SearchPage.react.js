import React, {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableHighlight,
    ActivityIndicatorIOS,
    Image,
    Component,
} from 'react-native';

import PropertyStore from '../stores/PropertyStore';
import PropertyActions from '../actions/PropertyActions';

const SearchResultsComponent = require('./SearchResults.react');

const styles = StyleSheet.create({
  description: {
    marginBottom: 20,
    fontSize: 18,
    textAlign: 'center',
    color: '#656565',
  },
  container: {
    padding: 30,
    marginTop: 65,
    alignItems: 'center',
  },
  flowRight: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center',
  },
  button: {
    height: 36,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  searchInput: {
    height: 36,
    padding: 4,
    marginRight: 5,
    flex: 4,
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#48BBEC',
    borderRadius: 8,
    color: '#48BBEC',
  },
  image: {
    width: 217,
    height: 138,
  },
});

class SearchPageComponent extends Component {

    constructor(props) {
      super(props);
      this.state = PropertyStore.getState();
      this.state.isLoading = false;
      this.state.searchString = 'london';
    }

    componentDidMount() {
      PropertyStore.listen(this.onChange.bind(this));
    }

    componentWillUnmount() {
      PropertyStore.unlisten(this.onChange.bind(this));
    }

    onChange(state) {
      this.setState(state);
      this.setState({ isLoading: false });
      if (state.errorMessage === null) {
        this.props.navigator.push({
          title: 'Results',
          component: SearchResultsComponent,
        });
      } else {
        this.setState({ errorMessage: 'Location not recognized; please try again.'});
      }
    }

    onSearchTextChanged(event) {
      this.setState({ searchString: event.nativeEvent.text });
    }

    _executeQuery(key, value) {
      this.setState({ isLoading: true });
      PropertyActions.getProperties(key, value);
    }

    onSearchPressed() {
      if (!this.state.isLoading) {
        this._executeQuery('place_name', this.state.searchString);
      }
    }

    onLocationPressed() {
      navigator.geolocation.getCurrentPosition(
          location => {
            const search = location.coords.latitude + ',' + location.coords.longitude;
            this.setState({ searchString: search });
            this._executeQuery('centre_point', search);
          },
          error => {
            this.setState({
              errorMessage: 'There was a problem with obtaining your location: ' + error,
            });
          });
    }

    render() {
      const spinner = this.state.isLoading ?
          ( <ActivityIndicatorIOS
              hidden="true"
              size="large"/> ) :
          ( <View/>);

      return (
            <View style={styles.container}>
                <Text style={styles.description}>
                    Search for houses to buy!
                </Text>
                <Text style={styles.description}>
                    Search by place-name, postcode or search near your location.
                </Text>
                <View style={styles.flowRight}>
                    <TextInput
                        style={styles.searchInput}
                        value={this.state.searchString}
                        onChange={this.onSearchTextChanged.bind(this)}
                        placeholder="Search via name or postcode"
                    />
                    <TouchableHighlight
                        style={styles.button}
                        onPress={this.onSearchPressed.bind(this)}
                        underlayColor="#99d9f4">
                        <Text style={styles.buttonText}>Go</Text>
                    </TouchableHighlight>
                </View>
                <TouchableHighlight
                    style={styles.button}
                    onPress={this.onLocationPressed.bind(this)}
                    underlayColor="#99d9f4">
                    <Text style={styles.buttonText}>Location</Text>
                </TouchableHighlight>
                <Image source={require('image!house')} style={styles.image}/>
                {spinner}
                <Text style={styles.description}>{this.state.errorMessage}</Text>
            </View>
        );
    }

}

module.exports = SearchPageComponent;
