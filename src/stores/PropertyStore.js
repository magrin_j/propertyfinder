import alt from '../alt';
import PropertyActions from '../actions/PropertyActions';

export class PropertyStore {

    constructor() {
      this.bindActions(PropertyActions);

      this.state = {
        properties: [],
        errorMessage: null,
      };
    }

    // Convetion de nomage 'on + premiere lettre maj' ou meme nom
    onUpdateProperties(obj) {
      const properties = obj; // Protect, avoid change!
      this.setState({ properties: properties, errorMessage: null });
    }

    onGetProperties() {
      this.setState({ properties: [] });
    }

    onPropertiesFailed(errorMessage) {
      this.setState({ errorMessage: errorMessage });
    }

}

export default alt.createStore(PropertyStore, 'PropertyStore');
