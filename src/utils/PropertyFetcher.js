function urlForQueryAndPage(key, value) {
  const data = {
    country: 'uk',
    pretty: '1',
    encoding: 'json',
    listing_type: 'buy',
    action: 'search_listings',
    page: 1,
  };
  data[key] = value;

  const querystring = Object.keys(data)
      .map(keyId => keyId + '=' + encodeURIComponent(data[keyId]))
      .join('&');

  return 'http://api.nestoria.co.uk/api?' + querystring;
}

export const PropertyFetcher = {
  fetch(key, value) {
    const query = urlForQueryAndPage(key, value);
    return fetch(query)
          .then(response => response.json())
          .then(json => { return json.response; });
  },
};
